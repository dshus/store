package store;
import store.Item;
import java.util.ArrayList;
import store.Store;
import java.util.Scanner;
import tinshoes.input.SafeScanner;
public class Main {
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		Store deli = new Store();
		ArrayList<Item> inventory = new ArrayList<Item>();
		for (Item sandwich : deli.getItems())
			System.out.println("Selling a " + sandwich.getDescription() + " for $" + sandwich.getBuyerPrice());
		System.out.println("Find us at " + deli.getLocation());
		System.out.println("Contact us at: " + deli.getPhone());
		System.out.println("Provided to you by " + deli.getOwner());
		double balance = sc.nextDouble("How much money do you have?", " is not valid input.", true);
		while (true) {
			System.out.println("$=$=$=$=$=$=$=$=$=$=$=$=$");
			System.out.println("Your balance: $" + balance);
			System.out.println("Type \"help\" for help and \"quit\" to quit.");
			System.out.println("$=$=$=$=$=$=$=$=$=$=$=$=$");
			String command = usc.next();
			switch (command.toLowerCase()) {
				case "buy":
					System.out.println("Which object would you like to buy?");
					usc.nextLine();
					String query = usc.nextLine();
					Item item = deli.find(query);
					if (item == null) {
						System.out.println("That item is not found.");
						break;
					}
					if (deli.buy(item, balance)) {
						balance -= item.getSalesPrice();
						System.out.println("The purchase was successful!");
						inventory.add(item);
					} else
						System.out.println("The purchase failed.");
					break;
				case "add":
					System.out.println("What is the description/name of the item?");
					usc.nextLine();
					String description = usc.nextLine();
					Item f = deli.find(description);
					if (f != null) {
						int n;
						do {
							n = sc.nextInt("How many would you like to add?", " is not valid input.", true);
							if (n < 0) System.out.println(n + " is not valid input.");
						} while (n < 0);
						for (int count = 0; count < n; count++) {
							deli.add(f);
						}
						System.out.println(description + " x" + n + " added to the store.");
						break;
					}			
					double buyer = sc.nextDouble("What is the buyer price of the item?", " is not valid input.", true);
					double sales = sc.nextDouble("What is the sales price of the item?", " is not valid input.", true);
					deli.add(new Item(description, buyer, sales));
					System.out.println("New item registered and added.");
					break;
				case "remove":
					System.out.println("Which object would you like to remove?");
					usc.nextLine();
					String target = usc.nextLine();
					Item i = deli.find(target);
					if (i == null) {
						System.out.println(target + " is not found.");
					}
					int r = sc.nextInt("How many would you like to remove?", " is not valid input.", true);
					if (r > deli.stock(i)) {
						System.out.println("There are not enough in stock to do that.");
						break;
					}
					for (int count2 = 0; count2 < r; count2++) {
						if (!deli.remove(i)) {
							System.out.println("A removal failed. Aborting...");
							break;
						}
					}
					System.out.println("Removal succeeded!");
					break;
				case "stock":
					System.out.println("Which object would you like to check?");
					usc.nextLine();
					String query2 = usc.nextLine();
					//System.out.println("======" + query2 + "======");
					Item j = deli.find(query2);
					if (j == null) {
						System.out.println(query2 + " is not found.");
						break;
					}
					System.out.println("There are " + query2 + " x" + deli.stock(j) + " in stock.");
					break;
				case "list":
					System.out.println("Listing...");
					for (Item k : deli.getItems()) {
						System.out.println(k);
					}
					break;
				case "inventory":
					System.out.println("Listing...");
					for (Item l : inventory) {
						System.out.println(l);
					}
					break;
				case "eat":
					System.out.println("What would you like to eat?");
					usc.nextLine();
					String food = usc.nextLine();
					Item b = null;
					for (Item p : inventory) {
						if (p.getDescription().equals(food)) {
							b = p;
							break;
						}
					}
					if (b == null) {
						System.out.println("You don't have " + food + "...");
					} else if (inventory.remove(b)) {
						System.out.println("Nom nom nom!");
					} else {
						System.out.println("You weren't able to eat.");
					}
					break;
				case "balance":
					balance += sc.nextDouble("How much money would you like to add/subtract from your balance? (Negatives to subtract)", " is not valid input.", true);
					break;
				case "help":
					System.out.println("COMMANDS:");
					System.out.println("\thelp: This help menu.");
					System.out.println("\tquit: Quit the program and leave the store.");
					System.out.println("\tadd: Add items to the store.");
					System.out.println("\tremove: Remove items from the store.");
					System.out.println("\tbuy: Buy items from the store.");
					System.out.println("\tlist: List the store's items.");
					System.out.println("\tstock: Check the stock of an item.");
					System.out.println("\tinventory: List your inventory.");
					System.out.println("\tbalance: Add or remove money from yourself.");
					System.out.println("\teat: Eat some food.");
					break;
				case "quit":
					System.out.println("Thank you for visiting us!");
					return;
				default:
					System.out.println("Command not recognized.");
					break;
			}
			System.out.println("$=$=$=$=$=$=$=$=$=$=$=$=$");
			System.out.println();
		}
	}
}
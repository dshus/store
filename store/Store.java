package store;
import store.Item;
import java.util.Arrays;
import java.util.ArrayList;
public class Store {
	private String location;
	private ArrayList<Item> items;
	private String owner;
	private String phone;
	public Store() {
		this("Millburn, NJ", new ArrayList<Item>(Arrays.asList(new Item[] {new Item("Griller #8", 9.25, 9.25), new Item("Godfadda", 9.75, 9.75), new Item("Gobbler", 8.75, 8.75)})), "Millburn Deli", "9733795800");
	}
	public Store(String location, ArrayList<Item> items, String owner, String phone) {
		this.location = location;
		this.items = items;
		this.owner = owner;
		this.phone = phone;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocation() {
		return this.location;
	}
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	public ArrayList<Item> getItems() {
		return this.items;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getOwner() {
		return this.owner;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPhone() {
		return this.phone;
	}
	//WARNING: Only returns the first item with that name...
	public Item find(String name) {
		for (Item item : items) {
			if (item.getDescription().equals(name)) return item;
		}
		return null;
	}
	public int stock(Item item) {
		int f = 0;
		for (Item j : items) {
			if (j.equals(item)) f++;
		}
		return f;
	}
	public void add(Item item) {
		items.add(item);
	}
	public boolean remove(Item item) {
		return items.remove(item);
	}
	//Returns whether the transaction was successful
	public boolean buy(Item item, double balance) {
		if (stock(item) == 0 || balance < item.getSalesPrice()) {
			return false;
		}
		remove(item);
		return true;
	}
}

package store;
public class Item {
	private String description;
	private double buyerPrice;
	private double salesPrice;
	public Item(String description, double buyerPrice, double salesPrice) {
		this.description = description;
		this.buyerPrice = buyerPrice;
		this.salesPrice = salesPrice;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescription() {
		return this.description;
	}
	public void setBuyerPrice(double buyerPrice) {
		this.buyerPrice = buyerPrice;
	}
	public double getBuyerPrice() {
		return this.buyerPrice;
	}
	public void setSalesPrice(double salesPrice) {
		this.salesPrice = salesPrice;
	}
	public double getSalesPrice() {
		return this.salesPrice;
	}
	public String toString() {
		return description + ", $" + salesPrice;
	}
}
